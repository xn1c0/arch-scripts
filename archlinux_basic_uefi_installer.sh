#!/usr/bin/env bash

# Basic archlinux install to run after pacstrap to achive a simplified setup
# Auth: xn1c0

# YOU MUST EDIT "Configuration" SECTION !!!

#******************************************#
# Packages you may want
#
# [Core functionality]
# efibootmgr dialog mtools dosfstools reflector base-devel linux-headers xdg-user-dirs xdg-utils bash-completion reflector
#
# [Kernel]
# linux-lts linux-lts-headers
#
# [Networking]
# networkmanager network-manager-applet wpa_supplicant avahi nss-mdns inetutils dnsutils openbsd-netcat iptables-nft ipset openssh firewalld rsync
#
# [Audio]
# alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack
#
# [Printing]
# cups hplip
#
# [Bluetooth]
# bluez bluez-utils
#
# [Virtualization]
# virt-manager qemu qemu-arch-extra edk2-ovmf bridge-utils dnsmasq vde2
#
# [Advanced Configuration and Power Interface]
# acpi acpi_call acpid #tlp
#
# [File manager functionality]
# gvfs gvfs-smb nfs-utils sof-firmware ntfs-3g
#
# [Fonts]
# terminus-font
#
# [Package - Booting]
# grub
#
# [Package - GPU]
# xf86-video-amdgpu
# nvidia nvidia-utils nvidia-settings

#******************************************#
# Configuration - edit at your needs!
time_zone="/usr/share/zoneinfo/Europe/Rome"
locale="en_US.UTF-8"
lang="LANG=${locale}"
keymap="KEYMAP=us"
hostname="archhost"
username="archuser"
root_pw="rootpw"
user_pw="userpw"
root_dev="/dev/vg/root" # /dev/vg/root
encrypt_open_name="crypt_volume_name" # crypt_volume_name
encrypt_UUID="" # Just the uuid
myHOOKS="HOOKS=(base systemd autodetect keyboard sd-vconsole modconf block sd-encrypt lvm2 filesystems fsck)"
is_encrypt_setup=true

#******************************************#
# System Configuration
ln -sf ${time_zone} /etc/localtime
hwclock --systohc

sed -i "0,/^#${locale}/s//${locale}/" /etc/locale.gen
locale-gen

echo $lang > /etc/locale.conf
echo $keymap > /etc/vconsole.conf
echo $hostname > /etc/hostname

echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 ${hostname}.localdomain ${hostname}" >> /etc/hosts

# Packages installation
pacman -Syy efibootmgr dialog mtools dosfstools reflector base-devel linux-headers xdg-user-dirs xdg-utils bash-completion reflector \
		linux-lts linux-lts-headers \
		networkmanager network-manager-applet wpa_supplicant avahi nss-mdns inetutils dnsutils openbsd-netcat nftables iptables-nft ipset openssh firewalld rsync \
		alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack \
		cups hplip \
		bluez bluez-utils \
		virt-manager qemu qemu-arch-extra edk2-ovmf bridge-utils dnsmasq vde2 \
		acpi acpi_call acpid \
		xf86-video-intel mesa \
		tlp tlp-rdw


# Ramdisk HOOKS
sed -i "0,/^HOOKS=(.*)/s//${myHOOKS}/" /etc/mkinitcpio.conf
mkinitcpio -p linux
mkinitcpio -p linux-lts

# XDG dirs
sed -i 's/^\([^#].*\)/# \1/g' /etc/xdg/user-dirs.defaults
# echo "DESKTOP=desktop" >> /etc/xdg/user-dirs.defaults
echo "TMP=tmp" >> /etc/xdg/user-dirs.defaults
echo "TRASH=tmp/trash" >> /etc/xdg/user-dirs.defaults
echo "DOWNLOAD=tmp/downloads" >> /etc/xdg/user-dirs.defaults
echo "DOCUMENTS=media/documents" >> /etc/xdg/user-dirs.defaults
echo "MUSIC=media/music" >> /etc/xdg/user-dirs.defaults
echo "PICTURES=media/pictures" >> /etc/xdg/user-dirs.defaults
# echo "PUBLICSHARE=public" >> /etc/xdg/user-dirs.defaults
# echo "TEMPLATES=templates" >> /etc/xdg/user-dirs.defaults
echo "VIDEOS=media/videos" >> /etc/xdg/user-dirs.defaults
echo "DEV=dev" >> /etc/xdg/user-dirs.defaults
echo "BIN=bin" >> /etc/xdg/user-dirs.defaults
echo "AUR_PKG=packages/aur" >> /etc/xdg/user-dirs.defaults
echo "HW_PKG=packages/hardware" >> /etc/xdg/user-dirs.defaults
echo "SW_PKG=packages/software" >> /etc/xdg/user-dirs.defaults
echo "OTHER_PKG=packages/other" >> /etc/xdg/user-dirs.defaults

# Boot Configuration
bootctl --path=/boot/ install

echo "default arch-*.conf" > /boot/loader/loader.conf
echo "timeout 5" >> /boot/loader/loader.conf
echo "console-mode 1" >> /boot/loader/loader.conf
echo "editor no" >> /boot/loader/loader.conf

title_main="Arch Linux MAIN"
title_lts="Arch Linux LTS"
options_default="root=${root_dev} loglevel=3 rd.systemd.show_status=auto rw"
if $is_encrypt_setup; then
	title_main="${title_main} - [ENCRYPTED]"
	title_lts="${title_lts} - [ENCRYPTED]"
	options_default="rd.luks.name=${encrypt_UUID}=${encrypt_open_name} ${options_default}"
fi

if [ $(pacman -Q | grep -c -w "^linux .*") -eq 1 ]; then
	echo "title ${title_main}" > /boot/loader/entries/arch-main.conf
	echo "linux /vmlinuz-linux" >> /boot/loader/entries/arch-main.conf
	echo "initrd /initramfs-linux.img" >> /boot/loader/entries/arch-main.conf
	echo "options ${options_default}" >> /boot/loader/entries/arch-main.conf
fi

if [ $(pacman -Q | grep -c -w "^linux-lts .*") -eq 1 ]; then
	echo "title ${title_lts}" > /boot/loader/entries/arch-lts.conf
	echo "linux /vmlinuz-linux-lts" >> /boot/loader/entries/arch-lts.conf
	echo "initrd /initramfs-linux-lts.img" >> /boot/loader/entries/arch-lts.conf
	echo "options ${options_default}" >> /boot/loader/entries/arch-lts.conf
fi

# Just in case of needs
# grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
# grub-mkconfig -o /boot/grub/grub.cfg

# Services
systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups.service
systemctl enable sshd
systemctl enable avahi-daemon
systemctl enable tlp
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable firewalld
systemctl enable acpid

# User Configuration
echo "root:${root_pw}" | chpasswd

# [ System - Setup new user ]
useradd -m "${username}"
echo "${username}:${user_pw}" | chpasswd
usermod -aG libvirt "${username}"

# [ System - Add new user to sudoers ]
echo "${username} ALL=(ALL) ALL" >> "/etc/sudoers.d/${username}"

# --------------------------------------------------------------------------------- #

echo "Done!"
