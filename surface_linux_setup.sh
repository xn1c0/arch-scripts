#!/usr/bin/env bash

# Surface Linux kernel setup
#  THIS WORKS WITH SYSTEMDBOOT AND ARCHLINUX_BASIC_UEFI_INSTALLER.SH !!!
# Auth: xn1c0

# Run this with sudo

# get curl pkg
pacman -Syy curl

# First you need to import the keys we use to sign packages.
curl -s https://raw.githubusercontent.com/linux-surface/linux-surface/master/pkg/keys/surface.asc \
		| sudo pacman-key --add -

pacman-key --finger 56C464BAAC421453
pacman-key --lsign-key 56C464BAAC421453

echo "[linux-surface]" >> /etc/pacman.conf
echo "Server = https://pkg.surfacelinux.com/arch/" >> /etc/pacman.conf

# refresh the repository metadata
pacman  -Syyu

# Install the linux-surface kernel and its dependencies
pacman -S linux-surface linux-surface-headers iptsd
systemctl enable iptsd

cp /boot/loader/entries/arch-main.conf /boot/loader/entries/arch-surface.conf
sed -i 's/MAIN/SURFACE/' /boot/loader/entries/arch-surface.conf
sed -i 's/-linux/-linux-surface/' /boot/loader/entries/arch-surface.conf

mkinitcpio -p linux-surface

gpg --keyserver hkps://keyserver.ubuntu.com --recv-key E23B7E70B467F0BF
sudo -u $USER git clone https://aur.archlinux.org/libwacom-surface.git ~/repos/aur
cd ~/packages/aur/libwacom-surface
sudo -u $USER makepkg -si

echo "MOZ_USE_XINPUT2=1" >> ~/.bash_profile

echo "Done!"
